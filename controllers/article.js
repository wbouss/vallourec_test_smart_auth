const ArticleService = require(`../services/ArticleService`);


module.exports = {
    getarticles: (req, res) => {
            ArticleService.getArticles()
                .then((data) => {
                    res.send(data);
                }).catch((err) => {
                res.send(err.message);
            });
    },
    getarticle: (req, res) => {
        if (!req.params.id) {
            res.send({
                code: 400,
            });
        } else {
            ArticleService.getArticle(req.params.id)
                .then((data) => {
                    res.send(data);
                }).catch((err) => {
                res.send(err.message);
            });
        }
    },
    createArticle: (req, res) => {
        ArticleService.createArticle({name: req.body.name , reference: req.body.reference , content: req.body.content, draft: req.body.draft, user:req.body.user })
            .then((data) => {
                res.send(data);
            }).catch((err) => {
            res.send(err.message);
        });
    },
};
