const UserService = require(`../services/UserService`);


module.exports = {
    createUser: (req, res) => {
        if (!req.body.username && !req.body.email && !req.body.password && !req.body.role) {
            res.send("Some informations are missing");
        } else {
            UserService.createUser({ username: req.body.username, email: req.body.email, password: req.body.password, role: req.body.role })
                .then((data) => {
                    res.send(data);
                }).catch((err) => {
                res.send(err.message);
            });
        }
    },
    login: (req, res) => {
        if (!req.body.username && !req.body.password ) {
            res.send("Some informations are missing");
        } else {
            UserService.login({ username: req.body.username, password: req.body.password})
                .then((data) => {
                    if(data)
                        res.send({
                            code: 200,
                            success: 'login ok'
                        });
                    else
                        res.send({
                            code: 204,
                            success: 'login ko'
                        });
                }).catch((err) => {
                res.send(err.message);
            });
        }
    }
};
