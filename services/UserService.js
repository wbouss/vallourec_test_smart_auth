const request = require(`request`);
const mysql = require('mysql');
const bcrypt = require('bcrypt');

module.exports = {
    getConnect: function() {
        var  db = mysql.createConnection({
            host: "127.0.0.1",
            user: "root",
            port: "3308",
            password: "mypassword",
            database: "userdbb"
        });
        return db;
    },
    createUser: function(userData) {
        return new Promise((resolve, reject) => {
            const con = this.getConnect();
            const salt =  bcrypt.genSalt(10, function (err, salt) {
                const password = bcrypt.hash(userData.password.toString(), salt , function (err, hash) {
                    var sqlQuery = "INSERT INTO user  (username, email, password, role) values ('"+userData.username+"','"+userData.email+"','"+hash+"', '"+userData.role+"')";
                    con.query( sqlQuery, function  (err, result)  {
                        if (err) throw err;
                        resolve(userData);
                    });
                });
            });
        })
    },
    login: function(userData) {
        return new Promise((resolve, reject) => {
            const con = this.getConnect();
            var sqlQuery = "SELECT username, password FROM user WHERE username = '"+ userData.username+"'";
            con.query( sqlQuery, function  (err, result)  {
                if (err) throw err;
                if(result.length > 0) {
                    const check =  bcrypt.compare( userData.password , result[0].password).then(value => {
                        resolve(value);
                    });
                }  else {
                    resolve(false)
                }
            });
        })
    },
};
