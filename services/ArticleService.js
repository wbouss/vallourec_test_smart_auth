const request = require(`request`);
const http = require('http');
const axios = require('axios')

const urlBase = 'http://127.0.0.1:8000/api/';

module.exports = {
    getArticles: function() {
        return new Promise((resolve, reject) => {
            http.get(urlBase+'articles' , resp => {
                let data = '';
                resp.on('data', (chunk) => {
                    data += chunk;
                    resolve(data);
                });
            });
        })
    },
    getArticle: function(id) {
        return new Promise((resolve, reject) => {
            http.get(urlBase+'articles/'+ id, resp => {
                let data = '';
                resp.on('data', (chunk) => {
                    data += chunk;
                    resolve(data);
                });
            });
        })
    },
    createArticle: function(article) {
        return new Promise((resolve, reject) => {

            axios.post(urlBase+'articles' , article).then(res => {
                resolve(res.data);
            })
            .catch(error => {
                reject(error);
            })
            });
    },
};
