const express = require('express');
const articleCtrl = require('../controllers/article');
const router = express.Router();
module.exports = router;

router.get('/articles',  articleCtrl.getarticles);
router.get('/articles/:id',  articleCtrl.getarticle);
router.post('/articles',  articleCtrl.createArticle);
