const express = require('express');
const userCtrl = require('../controllers/user');
const router = express.Router();
module.exports = router;

router.post('/create',  userCtrl.createUser);
router.post('/login', userCtrl.login)
