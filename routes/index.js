const express = require('express');
const userRoutes = require('./user');
const articleRoutes = require('./article');
const router = express.Router();
const articleCtrl = require('../controllers/article');

router.use('/user', userRoutes);
router.get('/articles',  articleCtrl.getarticles);
router.get('/articles/:id',  articleCtrl.getarticle);
router.post('/articles',  articleCtrl.createArticle);


module.exports = router;
